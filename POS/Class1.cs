﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS
{
    class Class1
    {
        public double Quantity { get; set; }
        public string Color { get; set; }
        public string ArticleName { get; set; }
        public double PU { get; set; }
        public double PT { get; set; }

        public Class1(double quantity, string articleName, double pu, string color)
        {
            this.Quantity = quantity;
            this.ArticleName = articleName;
            this.Color = color;
            this.PU = pu;
            this.PT = this.PU * this.Quantity;
        }
    }
}
