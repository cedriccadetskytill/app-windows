﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Tools
{
    abstract class ConsoleLogger
    {
        static int count = 0;

        public static void Log()
        {
            System.Diagnostics.Debug.WriteLine(count);
            ConsoleLogger.count++;
        }
        public static void Log(string popo)
        {
            System.Diagnostics.Debug.WriteLine(popo);
        }

        public static void Log<T>(List<T> popo)
        {
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(popo));
        }

        public static void Log<T>(Dictionary<string, T> popo)
        {
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(popo));
        }

        public static void Log<T>(T popo)
        {
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(popo));
        }
    }
}
