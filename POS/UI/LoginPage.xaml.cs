﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Realms;
using POS.Lib.Stories.Auth;
using POS.Lib.ExternalCommunication;
using Windows.UI.Popups;
using Newtonsoft.Json;
using System.Text;
using POS.Tools;
using POS.Lib.Stories.Synchronisation.Receive;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace POS
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        public LoginPage() { 
            InitializeComponent();
            LoginButton.Click += new RoutedEventHandler(AttemptLog);
            TokenTextBox.Text = "0b8995d1-d232-43b1-84f5-6c23c4786f2b";

            try
            {
                var realm = Realm.GetInstance();
                
                if (realm.All<Auth>().Count() > 0)
                    realm.Write(() => { realm.RemoveAll<Auth>(); });

                /*
                foreach (SynchronisationToAsk sync in realm.All<SynchronisationToAsk>())
                    realm.Write(() => { sync.asked = false; });
                */
                realm.Write(() => {
                    realm.RemoveAll<SynchronisationToAsk>();
                });

            }
            catch (Exception exc)
            {
                new MessageDialog("An Error occured").ShowAsync();
                return;
            }

            /*
            if (loggingDatas.Item1 == null)
                InitializeNoLogged();
            else
                InitializeAlreadyLogged(loggingDatas);
            
            LicenseTextBox.GotFocus += new RoutedEventHandler(TextBoxFocused);
            QuitKeyboardButton.Click += new RoutedEventHandler(CloseKeyboard);
            Keyboard.KeyPressed += new EventHandler<VirtualKeyEventArgs>(popo);
            */
        }

        private void AttemptLog(object sender, RoutedEventArgs e)
        {
           
            var realm = Realm.GetInstance();

            var token = TokenTextBox.Text;
            var cmnctor = new AuthServiceCommunicator();
            
                try
                {
                    var response = cmnctor.Login(token);
                    if (response.StatusCode == 0) {
                        new MessageDialog("Impossible de contacter le serveur SkyTill. Veuillez vérifier votre connexion internet.").ShowAsync();
                        return;
                    }
                    
                    EmptyTextBox(TokenTextBox);
                    var content = JsonConvert.DeserializeObject<Dictionary<string, string>>(response.Content);
                    
                    if (!response.IsSuccessful) {
                        new MessageDialog(new StringBuilder()
                            .AppendFormat("Wrong Access Token : {0}", content["message"])
                            .ToString()).ShowAsync();
                        EmptyTextBox(TokenTextBox);
                        return;
                    }
                    
                    if (!content.ContainsKey("api_token")) {
                        new MessageDialog("An error occured. Please contact your administrator.").ShowAsync();
                        EmptyTextBox(TokenTextBox);
                        return;
                    }
                    
                    try {
                        Auth infos = SavePosInfos(content["api_token"]);
                        ConsoleLogger.Log(new StringBuilder()
                            .AppendFormat("All right. Your api_token {0} has been recovered. POS is going to set up.",
                                infos.apiToken).ToString());


                        var syncCmnctor = new SyncServiceCommunicator();
                        var supersyncResponse = syncCmnctor.AskSuperSync();

                        if (response.StatusCode == 0) {
                            new MessageDialog("Impossible de contacter le serveur SkyTill. Veuillez vérifier votre connexion internet.").ShowAsync();
                            return;
                        }

                        if ((int)response.StatusCode != 200) {
                            new MessageDialog("Une erreur s'est produite lors de la demande de supersync.").ShowAsync();
                            return;
                        }

                    } catch (Exception exception) {
                        new MessageDialog("An error occured in setting of POS. Please contact your administrator")
                            .ShowAsync();
                        return;
                    }
            }
            catch (Exception exception) {
                new MessageDialog(new StringBuilder().AppendFormat("Exception handled : {0}", exception.Message).ToString()).ShowAsync();
                return;
            }
            ConsoleLogger.Log("aaaaah");
            LaunchPos();
        }

        private void EmptyTextBox(TextBox box)
        {
            //box.Text = "";
        }

        private Auth SavePosInfos(string token)
        {
            AuthController controller = new AuthController();
            Auth infos = controller.Create(token) ?? throw new Exception();
            return infos;
        }

        private void LaunchPos()
        {
            Frame.Navigate(typeof(POS.UI.MainPage));
        }

        /*
        private void TextBoxFocused(object sender, RoutedEventArgs e)
        {
            KeyboardView.Visibility = Visibility.Visible;
        }

        private void CloseKeyboard(object sender, RoutedEventArgs e)
        {
            KeyboardView.Visibility = Visibility.Hidden;
        }

        private void InitializeNoLogged()
        {
            this.LoginButton.Click += new RoutedEventHandler(this.PerformDistantLogin);
        }

        private void InitializeAlreadyLogged(Tuple<License, bool> loggingDatas)
        {
            License license = loggingDatas.Item1;
            if (license == null)
                this.LoginButton.Click += new RoutedEventHandler(this.PerformLocalLogin);
            else 
            {
                this.LicenseTextBox.Text = license.Number;
                if (loggingDatas.Item2)
                {
                    PasswordTextBox.Visibility = Visibility.Hidden;
                    this.LoginButton.Click += new RoutedEventHandler(RunSkyTill);
                }
                else
                {
                    this.LoginButton.Click += new RoutedEventHandler(this.PerformLocalLogin);
                }
            }
        }

        private void PerformLocalLogin(object sender, RoutedEventArgs e)
        {
            var password = this.PasswordTextBox.Text.ToString();

            if (password.Equals("password" Settings::Password()))
                RunSkyTill();
            else
            {
                PasswordTextBox.Text = "";
                MessageBox.Show("Wrong password");
            }
        }

        private void PerformDistantLogin(object sender, RoutedEventArgs e)
        {
            var license = this.LicenseTextBox.Text.ToString();
            var password = this.PasswordTextBox.Text.ToString();
            
            var cmnctor = new SkyTillServerCommunicator();
            switch (cmnctor.Login(license, password))
            {
                case 200:
                    this.RunSkyTill();
                    break;
                case 401:
                    MessageBox.Show("Il semblerait que le numéro de mot de passe ou le mot de passe soit incorrect.");
                    break;
                default:
                    MessageBox.Show("Une erreur inconnue est intervenue. Veuillez contacter l'équipe votre fournisseur SkyTill.");
                    break;
            }
        }
        private void RunSkyTill(object sender = null, RoutedEventArgs e = null)
        {
            this.Hide();
            var SkyTill = new MainWindow();
            SkyTill.Closed += (s, args) => this.Close();
            SkyTill.Show();
        }
        */
    }
}
