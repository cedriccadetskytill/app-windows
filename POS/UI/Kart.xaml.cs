﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d'élément Contrôle utilisateur, consultez la page https://go.microsoft.com/fwlink/?LinkId=234236

namespace POS.UI
{
    public sealed partial class Kart : ListView
    {
        public Kart()
        {
            List<Class1> Contents = new List<Class1> ();
            Contents.Add(new Class1(1, "popo", 1.45, "Yellow"));
            Contents.Add(new Class1(3, "lalalalaal", 3.36, "Yellow"));
            Contents.Add(new Class1(1, "un truc", 2.00, "Yellow"));
            Contents.Add(new Class1(4, "chocobons", 0.50, "Yellow"));
            Contents.Add(new Class1(1, "atrucbidule", 3.00, "Yellow"));

            this.ItemsSource = Contents;
        }
    }
}
