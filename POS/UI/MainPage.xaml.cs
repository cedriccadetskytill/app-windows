﻿using Windows.UI.Xaml.Controls;
using POS.Lib.Tasks;
using POS.Tools;
using System.Threading;

namespace POS.UI
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            new Synchronizer();
        }
    }
}
