﻿using System;
using System.Text;
using System.Threading;
using Windows.UI.Popups;
using POS.Lib.ExternalCommunication;
using POS.Tools;
using Newtonsoft.Json;
using POS.Lib.Stories.Synchronisation.Receive;
using System.Collections.Generic;

namespace POS.Lib.Tasks 
{
    public sealed class AskSyncsLoop: SyncLoop
    {
        
        private static readonly Lazy<AskSyncsLoop> lazy = new Lazy<AskSyncsLoop>(() => new AskSyncsLoop());

        public static AskSyncsLoop GetInstance
        {
            get { return lazy.Value; }
        }

        private AskSyncsLoop()
        {

            Cmnctor = new SyncServiceCommunicator();
            
            RefreshLabs = 2 * 1000;
        }

        public void Go()
        {               
            while (Thread.CurrentThread.IsAlive) {
                Thread.Sleep(RefreshLabs);
                this.Ask();
            }    
        }
        
        public void Ask()
        {
            var answer = Cmnctor.AskForSyncs(getMaxSync());
            if ((int)answer.StatusCode != 200)
            {
                ConsoleLogger.Log("status code => " + answer.StatusCode.ToString() + "  /  " + answer.Content.ToString());
                return;
            }

            var control = new SynchronisationToAskController();
            foreach (Dictionary<string, string> syncDict in 
                JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(answer.Content))
                control.Create(syncDict);
        }

        private int getMaxSync() {
           return  (new SynchronisationToAskController()).GetLast();
        }

        public Tuple<int, string> getStatus()
        {
            return new Tuple<int, string>(Purcent, Status);
        }
    }
}