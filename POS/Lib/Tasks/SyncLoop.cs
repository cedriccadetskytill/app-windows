﻿using POS.Lib.ExternalCommunication;

namespace POS.Lib.Tasks
{
    public abstract class SyncLoop
    {
        protected bool InSync;
        protected int RefreshLabs;
        protected int Counter;
        protected int MaxTimeLabs;
        
        protected int NbTotalSync;
        protected int NbSynced;
        protected int Purcent;
        protected string Status;
        
        protected SyncServiceCommunicator Cmnctor;

        public string Token;
        public bool Super;
        
        protected void Pause()
        {
            InSync = true;
        }

        protected void Resume()
        {
            Counter = 0;
            InSync = false;
        }
    }
}