﻿using System;
using System.Collections.Generic;
using System.Threading;
using POS.Lib.ExternalCommunication;
using POS.Tools;

namespace POS.Lib.Tasks
{
    public sealed class SendSyncLoop : SyncLoop
    {
        private Dictionary<string, string> _statusMatching = new Dictionary<string, string> 
        {
            {"Sale", "vente"}
        };
        
        private static readonly Lazy<SendSyncLoop> lazy = new Lazy<SendSyncLoop>(() => new SendSyncLoop());

        public static SendSyncLoop GetInstance
        {
            get { return lazy.Value; }
        }

        private SendSyncLoop()
        {
            Cmnctor = new SyncServiceCommunicator();

            Counter = 0;
            RefreshLabs = 7 * 1000;
            MaxTimeLabs = 15 * 1000;
        }

        public void Go()
        {
            while (Thread.CurrentThread.IsAlive) {
                //if (!InSync) {
                    Thread.Sleep(RefreshLabs);
                    //Counter += RefreshLabs;
                    //if (Counter >= MaxTimeLabs)
                        this.Send();
                }
        }

        public void Send()
        {
            // TODO: Take 50 first (server priority ranking) syncs to send to the server, and use SkyTillServerCommunicator  
        }

        private void Pause()
        {
            InSync = true;
        }

        private void Resume()
        {
            Counter = 0;
            InSync = false;
        }
        
        public Tuple<int, string> getStatus()
        {
            return new Tuple<int, string>(0, "");
        }
    }
}