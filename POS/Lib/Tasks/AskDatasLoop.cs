﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using POS.Lib.ExternalCommunication;
using POS.Lib.Stories.Synchronisation.Receive;
using POS.Tools;
using Realms;
using Windows.UI.Xaml.Media.Animation;

namespace POS.Lib.Tasks 
{
    public sealed class AskDataLoop : SyncLoop
    {
        static private List<string> CatalogServiceItems = new List<string> { "products", "categories", "subcategories", "catalogs" };
        static private List<string> AccountServiceItems = new List<string> { "licenses", "stores" };

        private Dictionary<string, List<string>> serviceMapper = new Dictionary<string, List<string>> {
            {"Catalog", CatalogServiceItems },
            {"Account", AccountServiceItems }
        };
        
        private static readonly Lazy<AskDataLoop> lazy = new Lazy<AskDataLoop>(() => new AskDataLoop());

        public static AskDataLoop GetInstance
        {
            get { return lazy.Value; }
        }

        private AskDataLoop()
        {
            Cmnctor = new SyncServiceCommunicator();

            InSync = false;
            RefreshLabs = 5 * 100 ;
            MaxTimeLabs = 15 * 1000;
        }
        
        public void Go()
        {
            Popo();
        }

        private async Task Popo()
        {
            while (Thread.CurrentThread.IsAlive)
            {
                Thread.Sleep(RefreshLabs);
                await Ask();
            }
        }

        public async Task Ask()
        {
            var control = new SynchronisationToAskController();
            var syncs = control.GetNexts();
//            ConsoleLogger.Log("nb syncs => " + syncs.Count);

            await Task.Run(() => Parallel.ForEach(syncs, sync =>
            {
                PerformDataAsking(sync);
            }));
        }

        /*
        private void performDataAsking(SynchronisationToAsk sync)
        {
            try
            { 
                string ServiceCommunicator = RetrieveService(sync.object_type);
                var type = Type.GetType("POS.Lib.ExternalCommunication."+ServiceCommunicator+"ServiceCommunicator");
                dynamic cmnctor = Activator.CreateInstance(type);
                
                // TODO: ASYNC CA MAMENE
                Thread.Sleep(5000);
                var response = cmnctor.Perform(sync);
                ConsoleLogger.Log(response.Content);
            }
            catch (Exception exc)
            {
                ConsoleLogger.Log(exc.Message);
            }
        }
        
        */
        
        private async Task<int> PerformDataAsking(int sync_id)
        {
            SynchronisationToAsk sync;
            try
            { 
                sync = (new SynchronisationToAskController()).Get(sync_id);
                string ServiceCommunicator = RetrieveService(sync.object_type);
                var type = Type.GetType("POS.Lib.ExternalCommunication." + ServiceCommunicator + "ServiceCommunicator");
                dynamic cmnctor = Activator.CreateInstance(type);

                var response = await cmnctor.Perform(sync);
                SynchronisationToAskRepository.setAsked(sync);

            }
            catch (Exception exc)
            {
                ConsoleLogger.Log("zeubi " + exc.Message);
                return -1;
            }

            try
            {
            
            }
            catch (Exception exc)
            {
                SynchronisationToAskRepository.setAsked(sync, false);
                return -1;
            }


            return 1;
        }
        
        private string RetrieveService(string sync_type)
        {
            ConsoleLogger.Log(sync_type);
            foreach (KeyValuePair<string, List<string>> service in serviceMapper)
                if (service.Value.Contains(sync_type))
                    return service.Key;
            ConsoleLogger.Log("gros caca");

            throw new Exception("GROS CACA");
        }
    }
}