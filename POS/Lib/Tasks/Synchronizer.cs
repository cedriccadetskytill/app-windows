﻿using System.Threading;

namespace POS.Lib.Tasks
{
    public delegate void ConsoleLogCallback(string popo);

    public class Synchronizer
    {
        public Synchronizer()
        {
            PerformAskSyncs();
            PerformAskDatas();
//            PerformSend();
        }
        
        public void PerformAskSyncs()
        {
            Thread InstanceCaller = new Thread(new ThreadStart(AskSyncsLoop.GetInstance.Go));
            InstanceCaller.Start();
        }
        
        public void PerformAskDatas()
        {
            Thread InstanceCaller = new Thread(new ThreadStart(AskDataLoop.GetInstance.Go));
            InstanceCaller.Start();
        }

        public void PerformSend()
        {
            Thread InstanceCaller = new Thread(new ThreadStart(SendSyncLoop.GetInstance.Go));
            InstanceCaller.Start();
        }
    }
}