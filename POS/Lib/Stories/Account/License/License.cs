﻿using Realms;

namespace POS.Lib.Stories.Account.License
{
    public class License: RealmObject
    {
        public string Number { get; set; }
        public int Id { get; set; }
    }
}