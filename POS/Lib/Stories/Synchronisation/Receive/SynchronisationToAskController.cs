﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using POS.Lib.Stories.Utils.Interfaces;

namespace POS.Lib.Stories.Synchronisation.Receive
{
    public class SynchronisationToAskController
    {
        protected SynchronisationToAskRepository CrudPerfomer;
        
        public SynchronisationToAskController()
        {
            CrudPerfomer = new SynchronisationToAskRepository();
        }


        public SynchronisationToAsk? Create(Dictionary<string, string> datas)
        {
            return CrudPerfomer.Create(datas) ?? throw new Exception();
        }

        public SynchronisationToAsk? Get(int id)
        {
            return CrudPerfomer.Read(id) ?? throw new Exception();
        }


        public List<int> GetNexts()
        {
            return CrudPerfomer.ReadLastToAsk();
        }

        public int GetLast()
        {
            return CrudPerfomer.ReadLastInMemory();
        }
    }
}