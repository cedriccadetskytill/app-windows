﻿using System;
using System.Collections.Generic;
using Realms;
using POS.Lib.Stories.Utils.Interfaces;
using Newtonsoft.Json;
using System.Linq;

namespace POS.Lib.Stories.Synchronisation.Receive
{
    public class SynchronisationToAskRepository
    {
        public SynchronisationToAsk? Create(Dictionary<string, string> datas)
        {
            var sync = new SynchronisationToAsk();
            JsonConvert.PopulateObject(JsonConvert.SerializeObject(datas), sync);
            sync.asked = false;

            var realm = Realm.GetInstance();
            
            try
            {
                realm.Write(() => { realm.Add(sync); });
                return sync;
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public SynchronisationToAsk? Read(int id)
        {
            var realm = Realm.GetInstance();
            return realm.Find<SynchronisationToAsk>(id);
        }

        public List<int> ReadLastToAsk()
        {
            var realm = Realm.GetInstance();
            List<int> syncs = new List<int>();
            foreach (SynchronisationToAsk sync in realm.All<SynchronisationToAsk>().Where(s => s.asked == false).ToList())
                syncs.Add(sync.id);

            return syncs;
        }

        public int ReadLastInMemory()
        {
            var realm = Realm.GetInstance();
            if (realm.All<SynchronisationToAsk>().Count() == 0)
                return 0;
            return realm.All<SynchronisationToAsk>().OrderByDescending(item => item.id).First().id;
        }

        public static void setAsked(SynchronisationToAsk sync, bool asked = true)
        {
            var realm = Realm.GetInstance();
            realm.Write(() => {
                sync.asked = asked;
            });
        }
    }
}