﻿using Realms;

namespace POS.Lib.Stories.Synchronisation.Receive
{
    public class SynchronisationToAsk: RealmObject
    {
        [PrimaryKey]
        public int id { get; set; }
        public string object_type { get; set; }
        public int object_id { get; set; }
        public bool asked { get; set; }

    }
}