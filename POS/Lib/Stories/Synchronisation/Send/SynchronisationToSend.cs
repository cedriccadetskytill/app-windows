﻿using Realms;

namespace POS.Lib.Stories.Synchronisation.Send
{
    public class SynchronisationToSend: RealmObject
    {
        public string object_type { get; set; }
        public int object_id { get; set; }
    }
}