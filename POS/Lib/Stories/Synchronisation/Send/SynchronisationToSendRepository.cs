﻿using System;
using System.Collections.Generic;
using Realms;
using POS.Lib.Stories.Utils.Interfaces;
using Newtonsoft.Json;

namespace POS.Lib.Stories.Synchronisation.Send
{
    public class SynchronisationToSendRepository: IPerformCrud
    {
        public RealmObject? Create(Dictionary<string, string> datas)
        {
            var sync = new SynchronisationToSend();
            JsonConvert.PopulateObject(JsonConvert.SerializeObject(datas), sync);

            var realm = Realm.GetInstance();

            try
            {
                realm.Write(() => { realm.Add(sync); });
                return sync;
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public RealmObject? ReadById(int id)
        {
            throw new NotImplementedException();
        }

        public List<RealmObject> Read(object datas)
        {
            throw new NotImplementedException();
        }

        public RealmObject? Update(int id, object datas)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}