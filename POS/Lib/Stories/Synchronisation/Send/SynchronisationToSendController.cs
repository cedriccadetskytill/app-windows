﻿
using System.Collections.Generic;
using POS.Lib.Stories.Utils.Interfaces;

namespace POS.Lib.Stories.Synchronisation.Send
{
    public class SynchronisationToSendController
    {
        protected IPerformCrud CrudPerfomer;
        
        public SynchronisationToSend? Create(string token)
        {
            var sync = CrudPerfomer.Create(new Dictionary<string, string>());
            
            return (SynchronisationToSend)sync;
        }
    }
}