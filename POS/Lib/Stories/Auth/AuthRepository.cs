﻿using System;
using System.Linq;
using POS.Tools;
using Realms;

namespace POS.Lib.Stories.Auth
{
    public class AuthRepository
    {
        public Auth? Create(string token)
        {
            var realm = Realm.GetInstance();
            try {
                Auth infos = new Auth {apiToken = token};
                realm.Write(() => { realm.Add(infos); });
                return infos;
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public Auth? Read()
        {
            var realm = Realm.GetInstance();

            try
            {
                return realm.All<Auth>().First();
            }
            catch (Exception exception)
            {
                return null;
            }
        }
        
    }
}