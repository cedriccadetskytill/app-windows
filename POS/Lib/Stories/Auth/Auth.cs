﻿using Realms;

namespace POS.Lib.Stories.Auth
{
    public class Auth: RealmObject
    {
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string apiToken { get; set; }
    }
}