﻿
using System;

namespace POS.Lib.Stories.Auth
{
    public class AuthController
    {
        protected AuthRepository CrudPerfomer;

        public AuthController()
        {
            CrudPerfomer = new AuthRepository();
        }
        
        public Auth? Create(string token)
        {
            var infos = CrudPerfomer.Create(token);
            return infos;
        }

        public Auth? Get()
        {
            return CrudPerfomer.Read() ?? throw new Exception();
        }
    }
}