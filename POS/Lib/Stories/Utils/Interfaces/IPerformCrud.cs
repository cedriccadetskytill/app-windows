﻿
using System.Collections.Generic;
using Realms;

namespace POS.Lib.Stories.Utils.Interfaces
{
    public interface IPerformCrud
    {
        public RealmObject? Create(Dictionary<string, string> datas);
        public RealmObject? ReadById(int id);
        public List<RealmObject> Read(object datas);
        public RealmObject? Update(int id, object datas);
        public void Delete(int id);
    }
}