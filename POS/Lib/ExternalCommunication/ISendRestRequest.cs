﻿using System.Collections.Generic;
using RestSharp;

namespace POS.Lib.ExternalCommunication
{
    public interface ISendRestRequest
    {
        //string DeleteSyncs(List<int> ids);
        //IRestResponse Login(string installToken);

        IRestResponse performRequest();
    }
}