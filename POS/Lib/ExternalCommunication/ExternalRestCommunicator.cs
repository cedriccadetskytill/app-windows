﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Popups;
using RestSharp;
using DataFormat = RestSharp.DataFormat;
using POS.Tools;
using RestSharp.Authenticators;

namespace POS.Lib.ExternalCommunication
{
    public class RestCommunicator: ISendRestRequest
    {
        private string ServerGatewayUri { get; set; }
        public string AdditionnalUri { get; set; }
        public string Method { get; set; }
        public Dictionary<string, string> UriParameters { get; set; }
        public Dictionary<string, string> Header { get; set; }
        private RestClient Client { get; set; }
        private RestRequest? Request { get; set; }
        private IRestResponse? Response { get; set; }

        public RestCommunicator(string uri)
        {
            this.ServerGatewayUri = uri;
            this.init();
        }

        private void init()
        {
            AdditionnalUri = "";
            Method = "";
            UriParameters = new Dictionary<string, string>();
            Header = new Dictionary<string, string>();
            Request = null;
            Response = null;
            try
            {
                Client = new RestClient(ServerGatewayUri);
            }
            catch (Exception e)
            {
                ConsoleLogger.Log(e.Message);
            }
        }
        
        public IRestResponse performRequest()
        {               
            if (Method.Equals("GET") || Method.Equals("DELETE"))
                foreach (var param in UriParameters)
                    AdditionnalUri = AdditionnalUri.Replace("{" + param.Key + "}", param.Value);
            
            Request = new RestRequest(AdditionnalUri);

            if (Method.Equals("POST") || Method.Equals("PATCH"))
            {
                Request.RequestFormat = DataFormat.Json;
                foreach (var param in UriParameters)
                    Request.AddParameter(param.Key, param.Value);
            }

            if (Header.ContainsKey("api_key"))
                Request.AddHeader("api_token", Header["api_key"]);

            Response = Method switch
            {
                "GET" => Client.Get(Request),
                "PATCH" => Client.Patch(Request),
                "POST" => Client.Post(Request),
                null => throw new Exception()
            };
            return Response;
        }
    }
}