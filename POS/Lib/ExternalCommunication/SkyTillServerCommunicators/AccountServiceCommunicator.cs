﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Popups;
using RestSharp;
using POS.Lib.Stories.Auth;
using POS.Lib.Stories.Synchronisation.Receive;
using System.Threading.Tasks;

namespace POS.Lib.ExternalCommunication
{
    public class AccountServiceCommunicator: SkyTillServerCommunicator
    {

        public AccountServiceCommunicator()
        {
            Communicator = new RestCommunicator(SkyTillServerUri);
            SetHeader();
            ((RestCommunicator)Communicator).Header = AuthKey;
        }

        public async Task<IRestResponse> Perform(SynchronisationToAsk sync)
        {
            ((RestCommunicator)Communicator).AdditionnalUri = "/{type}/{id}";
           ((RestCommunicator)Communicator).UriParameters = new Dictionary<string, string>() { { "type", sync.object_type }, { "id", sync.object_id.ToString() } };
            ((RestCommunicator)Communicator).Method = "GET";
            ((RestCommunicator)Communicator).Header = AuthKey;
            return Communicator.performRequest();
        }
    }
}