﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Popups;
using RestSharp;
using POS.Lib.Stories.Auth;
using POS.Lib.Stories.Synchronisation.Receive;

namespace POS.Lib.ExternalCommunication
{
    public class SyncServiceCommunicator: SkyTillServerCommunicator
    {

        public SyncServiceCommunicator()
        {
            Communicator = new RestCommunicator(SkyTillServerUri);
            SetHeader();
            ((RestCommunicator)Communicator).Header = AuthKey;
        }

        public IRestResponse AskForSyncs(int from_id)
        {
            ((RestCommunicator)Communicator).AdditionnalUri = "/syncs/?from={from}";
            ((RestCommunicator)Communicator).Method = "GET";
            ((RestCommunicator)Communicator).UriParameters= new Dictionary<string, string>() { { "from", from_id.ToString() } };

            return Communicator.performRequest();
        }

        public IRestResponse DeleteSyncs(List<int> ids)
        {
            ((RestCommunicator)Communicator).Method = "DELETE";
            ((RestCommunicator)Communicator).AdditionnalUri = "syncs/{ids}";
            ((RestCommunicator)Communicator).UriParameters["ids"] = String.Join("-", ids);
            return Communicator.performRequest();
        }
        
        public IRestResponse AskSuperSync()
        {
            ((RestCommunicator)Communicator).Method = "POST";
            ((RestCommunicator)Communicator).AdditionnalUri = "syncs/supersync";
            return Communicator.performRequest();
        }
   }
}