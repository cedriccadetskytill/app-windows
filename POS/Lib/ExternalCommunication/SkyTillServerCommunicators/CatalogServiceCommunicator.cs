﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Popups;
using RestSharp;
using POS.Lib.Stories.Auth;
using POS.Lib.Stories.Synchronisation.Receive;
using POS.Lib.Stories.Utils.Interfaces;
using POS.Lib.Stories.Catalog.Catalog;
using Windows.UI.Xaml.Media.Animation;
using System.Threading.Tasks;
using System.Threading;

namespace POS.Lib.ExternalCommunication
{
    public class CatalogServiceCommunicator: SkyTillServerCommunicator
    {

        public CatalogServiceCommunicator()
        {
            Communicator = new RestCommunicator(SkyTillServerUri);
            SetHeader();
        }

        public async Task<IRestResponse> Perform(SynchronisationToAsk sync)
        {
            Dictionary<string, string> uriParameters;
            if (sync.object_type.Equals("catalogs"))
            {
                ((RestCommunicator)Communicator).AdditionnalUri = "/catalogs/{catalog_id}/";
                uriParameters = new Dictionary<string, string>() { { "catalog_id", sync.object_id.ToString() } };
            }
            else
            {
                ((RestCommunicator)Communicator).AdditionnalUri = "/catalogs/{catalog_id}/{type}/{id}";
                uriParameters = new Dictionary<string, string>() { { "catalog_id", GetCurrentCatalog().Id.ToString() }, { "type", sync.object_type }, { "id", sync.object_id.ToString() } };
            }

            ((RestCommunicator)Communicator).UriParameters = uriParameters;
            ((RestCommunicator)Communicator).Method = "GET";
            ((RestCommunicator)Communicator).Header = AuthKey;
            return Communicator.performRequest();
        }

        private Catalog GetCurrentCatalog()
        {
            Catalog catalog = new Catalog();
            catalog.Id = 1;
            return catalog;
            // TODO: aller rechercher le premier enregistrement de catalogque dans la BDD Realm
        }
    }
}
