﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Popups;
using RestSharp;
using POS.Lib.Stories.Auth;
using POS.Lib.Stories.Synchronisation.Receive;

namespace POS.Lib.ExternalCommunication
{
    public class AuthServiceCommunicator: SkyTillServerCommunicator
    {

        public AuthServiceCommunicator() {
            Communicator = new RestCommunicator(SkyTillServerUri);
        }

        public IRestResponse Login(string installToken)
        {
            ((RestCommunicator)Communicator).Method = "POST";
            ((RestCommunicator)Communicator).AdditionnalUri = "api/login";
            ((RestCommunicator)Communicator).UriParameters["installation_token"] = installToken;
        
            var response = Communicator.performRequest();
            return response;
        }
    }
}