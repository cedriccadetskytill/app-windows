﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Popups;
using RestSharp;
using POS.Lib.Stories.Auth;
using POS.Lib.Stories.Synchronisation.Receive;
using POS.Tools;

namespace POS.Lib.ExternalCommunication
{
    abstract public class SkyTillServerCommunicator
    {
        protected ISendRestRequest Communicator;
        protected string SkyTillServerUri = "http://192.168.1.20:8000/";
        protected Dictionary<string, string> AuthKey = new Dictionary<string, string>();
        protected string ServiceUrl;

        protected void SetHeader()
        {
            AuthKey = new Dictionary<string, string>();
            var auth = new AuthController().Get();
            AuthKey.Add("api_key", auth.apiToken);
        }
    }
}